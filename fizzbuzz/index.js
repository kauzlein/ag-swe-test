/**
 *
 * Given is the following FizzBuzz application which counts from 1 to 100 and outputs either the
 * corresponding number or the corresponding text if one of the following rules apply.
 *
 * Rules:
 *  - dividable by 3 without a remainder -> Fizz
 *  - dividable by 5 without a remainder -> Buzz
 *  - dividable by 3 and 5 without a remainder -> FizzBuzz
 *
 * ACCEPTANCE CRITERIA:
 *
 * Please refactor this code so that it can be easily extended with other rules, such as
 * - "if it is dividable by 7 without a remainder output Bar"
 * - "if multiplied by 10 is larger than 100 output Foo"
 */

function fizzbuzz(limit = 100, rules) {
  for (let i = 1; i <= limit; i++) {
    let output = '';
    for (let rule of rules) {
      if (i % rule.divisor === 0 && (!rule.condition || rule.condition(i))) {
        output += rule.output;
      }
    }
    console.log(output || i);
  }
}

const rules = [
  { divisor: 3, output: 'Fizz' },
  { divisor: 5, output: 'Buzz' },
  { divisor: 7, output: 'Bar' },
  { divisor: 1, output: 'Foo', condition: (i) => i * 10 > 100 }
];

fizzbuzz(100, rules);
