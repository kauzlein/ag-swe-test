/**

 Print a pyramid!

      *
     ***
    *****
   *******
  *********

 ACCEPTANCE CRITERIA:

 Write a script to output pyramid of given size to the console (with leading spaces).

 */

function pyramid(size = 5) {
  if (size <= 0) {
    console.log('Invalid input size');
    return;
  }

  for (let i = 1; i <= size; i++) {
    let spaces = " ".repeat(size - i);
    let blocks = "*".repeat(i * 2 - 1);
    console.log(spaces + blocks + spaces);
  }
}

pyramid(5);
